# Scalar Few-Body Dynamics

This is a set of notes on the dynamics of few-body processes with scalar quanta.

Copyright (c) 2015 Melvin Eloy Irizarry-Gelpí