% Copyright (c) 2015 Melvin Eloy Irizarry-Gelpí
\chapter{Background Field Quantization}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The goal is to study many-body singularities in scattering amplitudes. As a first step, I will study scattering of scalar matter quanta interacting via the mediation of a massless scalar field. In order to obtain generic bound states I consider distinct scalar quanta. But first I discuss the details of a system with a single type of matter quanta coupled to a massless scalar field.

Consider a massive scalar \textit{quantum-mechanical} matter complex field $\red{\Phi}$ which I paint \red{red}. The matter field is coupled to a massless scalar wave real field $\periwinkle{A}$ which I paint \periwinkle{periwinkle}. I will split each field into a \textbf{background} and a \textbf{quantum} part:
\begin{equation}
	\red{\Phi} = \red{\varphi} + \red{\phi}, \qquad \periwinkle{A} = \periwinkle{B} + \periwinkle{Q}.
\end{equation}
Here $\red{\varphi}$ and $\periwinkle{B}$ are the \textbf{background} parts, and $\red{\phi}$ and $\periwinkle{Q}$ are the \textbf{quantum} parts. The action functional for the system is
\begin{equation}
	S[\red{\Phi}, \periwinkle{A}] = S_{\text{kin}}[\red{\phi}, \periwinkle{Q}] - S_{\text{int}}[\red{\Phi}, \periwinkle{A}],
\end{equation}
where $S_{\text{kin}}$ contains the kinetic terms for the \textbf{quantum} parts,
\begin{equation}
	S_{\text{kin}}[\red{\phi}, \periwinkle{Q}] = \int \mathrm{d}x \left[ \red{\phi}^{*} K_{\red{\Phi}} \red{\phi} + \frac{1}{2 g^{2}} \periwinkle{Q} K_{\periwinkle{A}} \periwinkle{Q} \right],
\end{equation}
and $S_{\text{int}}$ contains the interaction terms,
\begin{equation}
	S_{\text{int}}[\red{\Phi}, \periwinkle{A}] = \int \mathrm{d}x \left[ \red{q} \red{\Phi}^{*} \periwinkle{A} \red{\Phi} \right].
\end{equation}
Here $g$ is a dimensionful coupling strength and $\red{q}$ is a dimensionless charge. The kinetic operators are
\begin{equation}
	K_{\red{\Phi}} \equiv - \frac{1}{2} \hbar^{2} \partial^{2} + \frac{1}{2} \red{m}^{2}, \qquad K_{A} \equiv - \frac{1}{2} \partial^{2}
\end{equation}
Note the explicit factors of $\hbar$ in $K_{\red{\Phi}}$, which should remind you that $\red{\Phi}$ describes quantum-mechanical matter (i.e. particles).

The generating functional for truncated correlators is
\begin{equation}
	\mathcal{Z}_{\red{q}}[\red{\varphi}, \periwinkle{B}] = \int \widehat{\mathrm{D}}\red{\phi} \widehat{\mathrm{D}}\red{\phi}^{*} \widehat{\mathrm{D}}\periwinkle{Q} \, \exp{\left(- \frac{i}{\hbar} S[\red{\Phi}, \periwinkle{A}] \right)}
\end{equation}
Here the functional measures are normalized according to
\begin{equation}
	\widehat{\mathrm{D}}\red{\phi} \widehat{\mathrm{D}}\red{\phi}^{*} = \det{\left( \frac{i}{\hbar} K_{\red{\Phi}} \right)} \mathrm{D}\red{\phi} \mathrm{D}\red{\phi}^{*}
\end{equation}
and
\begin{equation}
	\widehat{\mathrm{D}}\periwinkle{Q} = \sqrt{\det{\left( \frac{i}{\hbar} K_{\periwinkle{A}} \right)}} \mathrm{D}\periwinkle{Q}
\end{equation}
The determinants here are functional determinants. This normalization leads to $\mathcal{Z}_{\red{q}}$ being dimensionless. In particular, for the free theory ($\red{q} = \red{0}$) you have $\mathcal{Z}_{\red{0}} = 1$.

I can now perform the functional integral over the matter field ($\red{\phi}$ and $\red{\phi}^{*}$). First I will rewrite the action functional $S$ as
\begin{equation}
	S = S_{\red{\phi}} + \frac{1}{2 g^{2}} \int \mathrm{d}x \left[\periwinkle{Q} K_{\periwinkle{A}} \periwinkle{Q} \right] - \red{q} \int \mathrm{d}x \left[ \red{\varphi}^{*} \periwinkle{A} \red{\varphi} \right]
\end{equation}
where I have collected all of the $\red{\phi}$ dependence into
\begin{equation}
	S_{\red{\phi}} = \int \mathrm{d}x \left[ \red{\phi}^{*} K_{\red{q}} \red{\phi} - \red{q} \red{\varphi}^{*} \periwinkle{A} \red{\phi} - \red{q} \red{\phi}^{*} \periwinkle{A} \red{\varphi}  \right], \qquad K_{\red{q}} \equiv K_{\red{\Phi}} - \red{q} \periwinkle{A}
\end{equation}
Note that you can write
\begin{equation}
	K_{\red{q}} = K_{\red{\Phi}} \left( I - \red{q} \periwinkle{A} G_{\red{\Phi}} \right)
\end{equation}
where $G_{\red{\Phi}} \equiv (K_{\red{\Phi}})^{-1}$ is the free massive scalar Green function.

The $S_{\red{\phi}}$ functional has the familiar ``sesquilinear plus linear'' form, so the functional integral over $\red{\phi}$ and $\red{\phi}^{*}$ can be done \textbf{exactly} to give
\begin{equation}
	\mathcal{Z}_{\red{q}}[\red{\varphi}, \periwinkle{B}] = \int \widehat{\mathrm{D}}\periwinkle{Q} \, \exp{\left( \red{\Upsilon}[\periwinkle{B} + \periwinkle{Q}] - \frac{i}{\hbar}S_{\periwinkle{Q}}[\red{\varphi}, \periwinkle{B}, \periwinkle{Q}]\right)}
\end{equation}
Here $\red{\Upsilon}$ involves an infinite set of one-loop contributions,
\begin{equation}
	\red{\Upsilon}[\periwinkle{A}] = \sum_{n = 1}^{\infty} \frac{1}{n} \red{\Upsilon}_{n}[\periwinkle{A}]
\end{equation}
with
\begin{equation}
	\red{\Upsilon}_{n}[\periwinkle{A}] \equiv \red{q}^{n} \int \mathrm{d}y_{1} \cdots \mathrm{d}y_{n} \, G_{\red{\Phi}}(y_{1}|y_{2}) \periwinkle{A}(y_{1}) \cdots G_{\red{\Phi}}(y_{n} | y_{1}) \periwinkle{A}(y_{n}) 
\end{equation}
The $S_{\periwinkle{Q}}$ functional is given by
\begin{equation}
	S_{\periwinkle{Q}} = \frac{1}{2 g^{2}} \int \mathrm{d}x \left[\periwinkle{Q} K_{\periwinkle{A}} \periwinkle{Q} \right] - \int \int \mathrm{d}x \mathrm{d}y \left[ \red{\varphi}^{*}(x) H_{\red{q}}(x|y) \red{\varphi}(y) \right]
\end{equation}
where $H_{\red{q}}$ depends on $\periwinkle{Q}$ through $\periwinkle{A} = \periwinkle{B} + \periwinkle{Q}$:
\begin{equation}
	H_{\red{q}}(x|y) \equiv \red{q}^{2} \periwinkle{A}(x) \periwinkle{A}(y) G_{\red{q}}(x|y) + \red{q} \periwinkle{A}(x) \delta(x - y)
\end{equation}
Here $G_{\red{q}} \equiv (K_{\red{q}})^{-1}$ is the Green function in the presence of the mediating field $\periwinkle{A}$.

Before looking at explicit examples of correlators, I will introduce a convenient notation for the functional integral over $\periwinkle{Q}$:
\begin{equation}
	\left\langle \cdots \right\rangle \equiv \int \widehat{\mathrm{D}}\periwinkle{Q} \left( \cdots \right) \exp{\left(\red{\Upsilon}[\periwinkle{B} + \periwinkle{Q}] - \frac{i}{2\hbar g^{2}} \int \mathrm{d}x \left[ \periwinkle{Q} K_{\periwinkle{A}} \periwinkle{Q} \right] \right)}
\end{equation}
Thus, the \textbf{exact} $\mathcal{Z}_{\red{q}}$ generating functional can be written as
\begin{equation}
	\mathcal{Z}_{\red{q}}[\red{\varphi}, \periwinkle{B}] = \left\langle \exp{\left( \frac{i}{\hbar} \int \int \mathrm{d}x \mathrm{d}y \left[ \red{\varphi}^{*}(x) H_{\red{q}}(x|y) \red{\varphi}(y) \right] \right)} \right\rangle \label{ZredAngles}
\end{equation}
Adding more matter fields is easy: for each extra matter field you get a corresponding $\Upsilon$ and a term $\varphi^{*} H_{q} \varphi$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Few-Body M{\o}ller Correlators}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M{\o}ller correlators have an equal number of matter (i.e. $\Phi$) and antimatter (i.e. $\Phi^{*}$) quanta.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{One-Body}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
You obtain correlators by taking functional derivatives of $\mathcal{Z}_{\red{q}}$ with respect to the background fields. The simplest correlator involves one kind of matter field:
\begin{equation}
	\mathcal{M}_{\red{\Phi}}(x_{\red{1}}, x_{\red{2}}) \equiv \lim_{\red{\varphi} \rightarrow 0} \lim_{\periwinkle{B} \rightarrow 0} \left[ \frac{\delta}{\delta \red{\varphi}^{*}(x_{\red{2}})} \frac{\delta}{\delta \red{\varphi}(x_{\red{1}})} \right] \mathcal{Z}_{\red{q}}
\end{equation}
This two-point correlator involves a single matter body, since it consist of an incoming $\red{\Phi}$ quantum and an outgoing $\red{\Phi}$ quantum. Using (\ref{ZredAngles}), you find
\begin{equation}
	\mathcal{M}_{\red{\Phi}}(x_{\red{1}}, x_{\red{2}}) = \left(\frac{i}{\hbar}\right) \left\langle H_{\red{q}}(x_{\red{2}} | x_{\red{1}}) \right\rangle
\end{equation}
More explicitly, you have $2^{1} = 2$ contributions:
\begin{equation}
	\left(\frac{\hbar}{i}\right) \mathcal{M}_{\red{\Phi}}(x_{\red{1}}, x_{\red{2}}) = \red{q}^{2} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\red{2}} G_{\red{q}}(x_{\red{2}} | x_{\red{1}}) \right\rangle + \red{q} \delta_{\red{2} \red{1}} \left\langle \periwinkle{Q}_{\red{1}} \right\rangle \label{CPhi}
\end{equation}
where I have adopted the short-hand notation $\periwinkle{Q}_{j} \equiv \periwinkle{Q}(x_{j})$ and $\delta_{jk} \equiv \delta(x_{j} - x_{k})$. Note that the first term in $\mathcal{M}_{\red{\Phi}}$ contains a factor of $G_{\red{q}}$. Recall that $G_{\red{q}}$ is the massive scalar Green function in the presence of the mediating field $\periwinkle{A}$:
\begin{equation}
	G_{\red{q}} = \left( -\frac{1}{2} \hbar^{2} \partial^{2} + \frac{1}{2} \red{m}^{2} - \red{q} \periwinkle{A} \right)^{-1}
\end{equation}
You can write this Green function as a functional integral over a path variable (i.e. a path integral for a particle),
\begin{equation}
	G_{\red{q}}(x_{\red{2}} | x_{\red{1}}) = \int\limits_{0}^{\infty} \mathrm{d}\red{T} \int\limits_{x_{\red{1}}}^{x_{\red{2}}} \mathrm{D}\red{x}(\red{\tau}) \, \exp{\left(- \frac{i}{\hbar} S_{\red{x}}[\red{x}, \periwinkle{A}] \right)}
\end{equation}
with the functional $S_{\red{x}}$ given by
\begin{equation}
	S_{\red{x}}[\red{x}, \periwinkle{A}] = \int \mathrm{d}\red{\tau} \left(- \frac{1}{2} \red{\dot{x}}^{2} + \frac{1}{2} \red{m}^{2} + \red{q} \periwinkle{A}[\red{x}(\red{\tau})] \right)
\end{equation}
Later I will study each term in (\ref{CPhi}) in more detail.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Two-Body}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In order to study a more general set of two-body bound states, I will consider the two-body correlator for a system with two distinct quantum matter fields, which are painted \red{red} and \blue{blue}. The generating functional becomes
\begin{equation}
	\mathcal{Z}_{\red{q} \blue{q}}[\red{\varphi}, \blue{\varphi}, \periwinkle{B}] = \int \widehat{\mathrm{D}}\periwinkle{Q} \, \exp{\left(- \frac{i}{\hbar} S_{\periwinkle{Q}}[\red{\varphi}, \blue{\varphi}, \periwinkle{Q}] + \red{\Upsilon}[\periwinkle{B} + \periwinkle{Q}] + \blue{\Upsilon}[\periwinkle{B} + \periwinkle{Q}] \right)} \label{Zredblue}
\end{equation}
with the $S_{\periwinkle{Q}}$ functional now given by
\begin{equation}
	S_{\periwinkle{Q}} = \frac{1}{2 g^{2}} \int \mathrm{d}x \left[ \periwinkle{Q} K_{\periwinkle{A}} \periwinkle{Q} \right] - \int \int \mathrm{d}x \mathrm{d}y \left[ \red{\varphi}^{*}(x) H_{\red{q}}(x|y) \red{\varphi}(y) + \blue{\varphi}^{*}(x) H_{\blue{q}}(x|y) \blue{\varphi}(y) \right]
\end{equation}
In a more compact notation, I will write
\begin{equation}
	\mathcal{Z}_{\red{q} \blue{q}}[\red{\varphi}, \blue{\varphi}, \periwinkle{B}] = \left\langle \exp{\left( \frac{i}{\hbar} \int \int \mathrm{d}x \mathrm{d}y \left[ \red{\varphi}^{*}(x) H_{\red{q}}(x|y) \red{\varphi}(y) + \blue{\varphi}^{*}(x) H_{\blue{q}}(x|y) \blue{\varphi}(y) \right] \right)} \right\rangle
\end{equation}
The two-body correlator corresponds to the four-point correlator given by
\begin{equation}
	\mathcal{M}_{\red{\Phi} \blue{\Phi}}(x_{\red{1}}, x_{\blue{2}}, x_{\red{3}}, x_{\blue{4}}) \equiv \lim_{\red{\varphi} \rightarrow 0} \lim_{\blue{\varphi} \rightarrow 0} \lim_{\periwinkle{B} \rightarrow 0} \left[ \frac{\delta}{\delta \red{\varphi}^{*}(x_{\red{3}})} \frac{\delta}{\delta \blue{\varphi}^{*}(x_{\blue{4}})} \frac{\delta}{\delta \red{\varphi}(x_{\red{1}})} \frac{\delta}{\delta \blue{\varphi}(x_{\blue{2}})} \right] \mathcal{Z}_{\red{q} \blue{q}}
\end{equation}
After applying the functional derivatives, you obtain
\begin{equation}
	\mathcal{M}_{\red{\Phi} \blue{\Phi}}(x_{\red{1}}, x_{\blue{2}}, x_{\red{3}}, x_{\blue{4}}) = \left( \frac{i}{\hbar} \right)^{2} \left\langle H_{\red{q}}(x_{\red{3}} | x_{\red{1}}) H_{\blue{q}}(x_{\blue{4}}| x_{\blue{2}}) \right\rangle \label{HredHblue}
\end{equation}
More explicitly, you have $2^{2} = 4$ contributions:
\begin{align}
	\left( \frac{\hbar}{i} \right)^{2} \mathcal{M}_{\red{\Phi} \blue{\Phi}}(x_{\red{1}}, x_{\blue{2}}, x_{\red{3}}, x_{\blue{4}}) = {}& \red{q}^{2} \blue{q}^{2} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\red{3}} \periwinkle{Q}_{\blue{4}} G_{\red{q}}(x_{\red{3}} | x_{\red{1}}) G_{\blue{q}}(x_{\blue{4}}| x_{\blue{2}}) \right\rangle \nonumber \\
	&+ \red{q}^{2} \blue{q} \delta_{\blue{4}\blue{2}} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\red{3}} G_{\red{q}}(x_{\red{3}} | x_{\red{1}}) \right\rangle \nonumber \\
	&+ \red{q} \blue{q}^{2} \delta_{\red{3} \red{1}} \left\langle \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\red{3}} \periwinkle{Q}_{\blue{4}} G_{\blue{q}}(x_{\blue{4}}| x_{\blue{2}}) \right\rangle \nonumber \\
	&+ \red{q} \blue{q} \delta_{\red{3} \red{1}} \delta_{\blue{4} \blue{2}} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \right\rangle
\end{align}
The first line corresponds to a two-body path integral. The second and third lines correspond to one-body path integrals, each over a different body. The tree-level contribution comes from the last line.

You should substract from $\mathcal{M}_{\red{\Phi} \blue{\Phi}}$ the disconnected part,
\begin{equation}
	\mathcal{D}_{\red{\Phi} \blue{\Phi}}(x_{\red{1}}, x_{\blue{2}}, x_{\red{3}}, x_{\blue{4}}) \equiv \mathcal{M}_{\red{\Phi}}(x_{\red{1}}, x_{\red{3}}) \mathcal{M}_{\blue{\Phi}}(x_{\blue{2}}, x_{\blue{4}})
\end{equation}
More explicitly, you have $2^{2} = 4$ contributions:
\begin{align}
	\left( \frac{\hbar}{i} \right)^{2} \mathcal{D}_{\red{\Phi} \blue{\Phi}}(x_{\red{1}}, x_{\blue{2}}, x_{\red{3}}, x_{\blue{4}}) = {}& \red{q}^{2} \blue{q}^{2} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\red{3}} G_{\red{q}}(x_{\red{3}} | x_{\red{1}}) \right\rangle \left\langle \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\blue{4}} G_{\blue{q}}(x_{\blue{4}}| x_{\blue{2}}) \right\rangle \nonumber \\
	&+ \red{q}^{2} \blue{q} \delta_{\blue{4}\blue{2}} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\red{3}} G_{\red{q}}(x_{\red{3}} | x_{\red{1}}) \right\rangle \left\langle \periwinkle{Q}_{\blue{2}} \right\rangle \nonumber \\
	&+ \red{q} \blue{q}^{2} \delta_{\red{3} \red{1}} \left\langle \periwinkle{Q}_{\red{3}} \right\rangle \left\langle \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\blue{4}} G_{\blue{q}}(x_{\blue{4}}| x_{\blue{2}}) \right\rangle \nonumber \\
	&+ \red{q} \blue{q} \delta_{\red{3} \red{1}} \delta_{\blue{4} \blue{2}} \left\langle \periwinkle{Q}_{\red{1}} \right\rangle \left\langle \periwinkle{Q}_{\blue{2}} \right\rangle
\end{align}
Defining the connected correlator as
\begin{equation}
	\mathcal{G}_{\red{\Phi} \blue{\Phi}}(x_{\red{1}}, x_{\blue{2}}, x_{\red{3}}, x_{\blue{4}}) \equiv \mathcal{M}_{\red{\Phi} \blue{\Phi}}(x_{\red{1}}, x_{\blue{2}}, x_{\red{3}}, x_{\blue{4}}) - \mathcal{D}_{\red{\Phi} \blue{\Phi}}(x_{\red{1}}, x_{\blue{2}}, x_{\red{3}}, x_{\blue{4}})
\end{equation}
you find

\begin{equation}
...
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Three-Body}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Now I add another matter field to the previous system, which I paint \green{green}. This leads to a generating functional $\mathcal{Z}_{\red{q} \blue{q} \green{q}}$ which is very similar to (\ref{Zredblue}). I will study the three-body correlator that corresponds to the six-point correlator
\begin{equation}
	\mathcal{M}_{\red{\Phi} \blue{\Phi} \green{\Phi}} \equiv \lim_{\red{\varphi} \rightarrow 0} \lim_{\blue{\varphi} \rightarrow 0} \lim_{\green{\varphi} \rightarrow 0} \lim_{\periwinkle{B} \rightarrow 0} \left[ \frac{\delta}{\delta \red{\varphi}^{*}(x_{\red{4}})} \frac{\delta}{\delta \blue{\varphi}^{*}(x_{\blue{5}})} \frac{\delta}{\delta \green{\varphi}^{*}(x_{\green{6}})} \frac{\delta}{\delta \red{\varphi}(x_{\red{1}})} \frac{\delta}{\delta \blue{\varphi}(x_{\blue{2}})} \frac{\delta}{\delta \green{\varphi}(x_{\green{3}})} \right] \mathcal{Z}_{\red{q} \blue{q} \green{q}}
\end{equation}
In analogy with (\ref{HredHblue}), you can write $\mathcal{M}_{\red{\Phi} \blue{\Phi} \green{\Phi}}$ as
\begin{equation}
	\mathcal{M}_{\red{\Phi} \blue{\Phi} \green{\Phi}}(x_{\red{1}}, x_{\blue{2}}, x_{\green{3}}, x_{\red{4}}, x_{\blue{5}}, x_{\green{6}}) = \left( \frac{i}{\hbar} \right)^{3} \left\langle H_{\red{q}}(x_{\red{4}} | x_{\red{1}}) H_{\blue{q}}(x_{\blue{5}}| x_{\blue{2}}) H_{\green{q}}(x_{\green{6}}| x_{\green{3}}) \right\rangle
\end{equation}
More explicitly, you have $2^{3} = 8$ contributions:
\begin{align}
\left( \frac{\hbar}{i} \right)^{3} \mathcal{M}_{\red{\Phi} \blue{\Phi} \green{\Phi}} = {}& \red{q}^{2} \blue{q}^{2} \green{q}^{2} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\blue{5}} \periwinkle{Q}_{\green{6}} G_{\red{q}}(x_{\red{4}} | x_{\red{1}}) G_{\blue{q}}(x_{\blue{5}}| x_{\blue{2}}) G_{\green{q}}(x_{\green{6}}| x_{\green{3}}) \right\rangle \nonumber \\
	&+ \red{q}^{2} \blue{q}^{2} \green{q} \delta_{\green{63}} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\blue{5}} G_{\red{q}}(x_{\red{4}} | x_{\red{1}}) G_{\blue{q}}(x_{\blue{5}}| x_{\blue{2}}) \right\rangle \nonumber \\
	&+ \red{q}^{2} \blue{q} \green{q}^{2} \delta_{\blue{52}} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\green{6}} G_{\red{q}}(x_{\red{4}} | x_{\red{1}}) G_{\green{q}}(x_{\green{6}}| x_{\green{3}}) \right\rangle \nonumber \\
	&+ \red{q} \blue{q}^{2} \green{q}^{2} \delta_{\red{41}} \left\langle \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\blue{5}} \periwinkle{Q}_{\green{6}} G_{\blue{q}}(x_{\blue{5}}| x_{\blue{2}}) G_{\green{q}}(x_{\green{6}}| x_{\green{3}}) \right\rangle \nonumber \\
	&+ \red{q}^{2} \blue{q} \green{q} \delta_{\blue{52}} \delta_{\green{63}} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} G_{\red{q}}(x_{\red{4}} | x_{\red{1}}) \right\rangle \nonumber \\
	&+ \red{q} \blue{q}^{2} \green{q} \delta_{\red{41}} \delta_{\green{63}} \left\langle \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\blue{5}} G_{\blue{q}}(x_{\blue{5}}| x_{\blue{2}}) \right\rangle \nonumber \\
	&+ \red{q} \blue{q} \green{q}^{2} \delta_{\red{41}} \delta_{\blue{52}} \left\langle \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\blue{5}} \periwinkle{Q}_{\green{6}} G_{\green{q}}(x_{\green{6}}| x_{\green{3}}) \right\rangle \nonumber \\
	&+ \red{q} \blue{q} \green{q} \delta_{\red{41}} \delta_{\blue{52}} \delta_{\green{63}} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \right\rangle
\end{align}
You expect the tree-level contribution to arise from the terms in lines 5, 6 and 7.

You should substract from $\mathcal{M}_{\red{\Phi} \blue{\Phi} \green{\Phi}}$ the disconnected part,
\begin{equation}
	\mathcal{D}_{\red{\Phi} \blue{\Phi} \green{\Phi}} \equiv \mathcal{M}_{\red{\Phi}} \mathcal{M}_{\blue{\Phi} \green{\Phi}} + \mathcal{M}_{\blue{\Phi}} \mathcal{M}_{\red{\Phi} \green{\Phi}} + \mathcal{M}_{\green{\Phi}} \mathcal{M}_{\red{\Phi} \blue{\Phi}} -2 \mathcal{M}_{\red{\Phi}} \mathcal{M}_{\blue{\Phi}} \mathcal{M}_{\green{\Phi}}
\end{equation}
which can also be written as
\begin{equation}
	\mathcal{D}_{\red{\Phi} \blue{\Phi} \green{\Phi}} = \mathcal{M}_{\red{\Phi}} \mathcal{G}_{\blue{\Phi} \green{\Phi}} + \mathcal{M}_{\blue{\Phi}} \mathcal{G}_{\red{\Phi} \green{\Phi}} + \mathcal{M}_{\green{\Phi}} \mathcal{G}_{\red{\Phi} \blue{\Phi}} + \mathcal{M}_{\red{\Phi}} \mathcal{M}_{\blue{\Phi}} \mathcal{M}_{\green{\Phi}}
\end{equation}
where $\mathcal{G}_{\Phi \Phi}$ is a connected two-body correlator, e.g.
\begin{equation}
	\mathcal{G}_{\red{\Phi} \blue{\Phi}} = \mathcal{M}_{\red{\Phi} \blue{\Phi}} - \mathcal{M}_{\red{\Phi}} \mathcal{M}_{\blue{\Phi}}
\end{equation}
More explicitly, you have $2^{3} = 8$ contributions:
\begin{align}
\left( \frac{\hbar}{i} \right)^{3} \mathcal{D}_{\red{\Phi} \blue{\Phi} \green{\Phi}} = {}& \red{q}^{2} \blue{q}^{2} \green{q}^{2} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\blue{5}} \periwinkle{Q}_{\green{6}} G_{\red{q}}(x_{\red{4}} | x_{\red{1}}) G_{\blue{q}}(x_{\blue{5}}| x_{\blue{2}}) G_{\green{q}}(x_{\green{6}}| x_{\green{3}}) \right\rangle \nonumber \\
	&+ \red{q}^{2} \blue{q}^{2} \green{q} \delta_{\green{63}} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\blue{5}} G_{\red{q}}(x_{\red{4}} | x_{\red{1}}) G_{\blue{q}}(x_{\blue{5}}| x_{\blue{2}}) \right\rangle \nonumber \\
	&+ \red{q}^{2} \blue{q} \green{q}^{2} \delta_{\blue{52}} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\green{6}} G_{\red{q}}(x_{\red{4}} | x_{\red{1}}) G_{\green{q}}(x_{\green{6}}| x_{\green{3}}) \right\rangle \nonumber \\
	&+ \red{q} \blue{q}^{2} \green{q}^{2} \delta_{\red{41}} \left\langle \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\blue{5}} \periwinkle{Q}_{\green{6}} G_{\blue{q}}(x_{\blue{5}}| x_{\blue{2}}) G_{\green{q}}(x_{\green{6}}| x_{\green{3}}) \right\rangle \nonumber \\
	&+ \red{q}^{2} \blue{q} \green{q} \delta_{\blue{52}} \delta_{\green{63}} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} G_{\red{q}}(x_{\red{4}} | x_{\red{1}}) \right\rangle \nonumber \\
	&+ \red{q} \blue{q}^{2} \green{q} \delta_{\red{41}} \delta_{\green{63}} \left\langle \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\blue{5}} G_{\blue{q}}(x_{\blue{5}}| x_{\blue{2}}) \right\rangle \nonumber \\
	&+ \red{q} \blue{q} \green{q}^{2} \delta_{\red{41}} \delta_{\blue{52}} \left\langle \periwinkle{Q}_{\green{3}} \periwinkle{Q}_{\red{4}} \periwinkle{Q}_{\blue{5}} \periwinkle{Q}_{\green{6}} G_{\green{q}}(x_{\green{6}}| x_{\green{3}}) \right\rangle \nonumber \\
	&+ \red{q} \blue{q} \green{q} \delta_{\red{41}} \delta_{\blue{52}} \delta_{\green{63}} \left\langle \periwinkle{Q}_{\red{1}} \periwinkle{Q}_{\blue{2}} \periwinkle{Q}_{\green{3}} \right\rangle
\end{align}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Four-Body}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The last system that I will study has four distinct matter fields. I will pay tribute to the \cyan{C}\magenta{M}\yellow{Y}\gray{K} color model and paint the four matter fields \cyan{cyan}, \magenta{magenta}, \yellow{yellow} and \gray{gray}. Thus, you have a generating functional $\mathcal{Z}_{\cyan{q} \magenta{q} \yellow{q} \gray{q}}$ and the four-body correlator follows from the eight-point correlator
\begin{equation}
	\mathcal{M}_{\cyan{\Phi} \magenta{\Phi} \yellow{\Phi} \gray{\Phi}} \equiv \left[ \frac{\delta}{\delta \cyan{\varphi}^{*}(x_{\cyan{5}})} \frac{\delta}{\delta \magenta{\varphi}^{*}(x_{\magenta{6}})} \frac{\delta}{\delta \yellow{\varphi}^{*}(x_{\yellow{7}})} \frac{\delta}{\delta \gray{\varphi}^{*}(x_{\gray{8}})} \frac{\delta}{\delta \cyan{\varphi}(x_{\cyan{1}})} \frac{\delta}{\delta \magenta{\varphi}(x_{\magenta{2}})} \frac{\delta}{\delta \yellow{\varphi}(x_{\yellow{3}})} \frac{\delta}{\delta \gray{\varphi}(x_{\gray{4}})} \right] \mathcal{Z}_{\cyan{q} \magenta{q} \yellow{q} \gray{q}}
\end{equation}
with the appropriate limits being taken implicitly. It should be no surprise that you can write $\mathcal{M}_{\cyan{\Phi} \magenta{\Phi} \yellow{\Phi} \gray{\Phi}}$ as
\begin{equation}
	\mathcal{M}_{\cyan{\Phi} \magenta{\Phi} \yellow{\Phi} \gray{\Phi}} = \left( \frac{i}{\hbar} \right)^{4} \left\langle H_{\cyan{q}}(x_{\cyan{5}} | x_{\cyan{1}}) H_{\magenta{q}}(x_{\magenta{6}}| x_{\magenta{2}}) H_{\yellow{q}}(x_{\yellow{7}}| x_{\yellow{3}}) H_{\gray{q}}(x_{\gray{8}}|x_{\gray{4}}) \right\rangle
\end{equation}
More explicitly, you have $2^{4} = 16$ contributions:
\begin{align}
	\left( \frac{\hbar}{i} \right)^{4} \mathcal{M}_{\cyan{\Phi} \magenta{\Phi} \yellow{\Phi} \gray{\Phi}} = {}& \cyan{q}^{2} \magenta{q}^{2} \yellow{q}^{2} \gray{q}^{2} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\cyan{5}} \periwinkle{Q}_{\magenta{6}} \periwinkle{Q}_{\yellow{7}} \periwinkle{Q}_{\gray{8}} G_{\cyan{q}}(x_{\cyan{5}} | x_{\cyan{1}}) G_{\magenta{q}}(x_{\magenta{6}}| x_{\magenta{2}}) G_{\yellow{q}}(x_{\yellow{7}}| x_{\yellow{3}}) G_{\gray{q}}(x_{\gray{8}}|x_{\gray{4}}) \right\rangle \nonumber \\
	&+ \cyan{q} \magenta{q}^{2} \yellow{q}^{2} \gray{q}^{2} \delta_{\cyan{51}} \left\langle \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\cyan{5}} \periwinkle{Q}_{\magenta{6}} \periwinkle{Q}_{\yellow{7}} \periwinkle{Q}_{\gray{8}} G_{\magenta{q}}(x_{\magenta{6}}| x_{\magenta{2}}) G_{\yellow{q}}(x_{\yellow{7}}| x_{\yellow{3}}) G_{\gray{q}}(x_{\gray{8}}|x_{\gray{4}}) \right\rangle \nonumber \\
	&+ \cyan{q}^{2} \magenta{q} \yellow{q}^{2} \gray{q}^{2} \delta_{\magenta{62}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\cyan{5}} \periwinkle{Q}_{\magenta{6}} \periwinkle{Q}_{\yellow{7}} \periwinkle{Q}_{\gray{8}} G_{\cyan{q}}(x_{\cyan{5}} | x_{\cyan{1}}) G_{\yellow{q}}(x_{\yellow{7}}| x_{\yellow{3}}) G_{\gray{q}}(x_{\gray{8}}|x_{\gray{4}}) \right\rangle \nonumber \\
	&+ \cyan{q}^{2} \magenta{q}^{2} \yellow{q} \gray{q}^{2} \delta_{\yellow{73}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\cyan{5}} \periwinkle{Q}_{\magenta{6}} \periwinkle{Q}_{\yellow{7}} \periwinkle{Q}_{\gray{8}} G_{\cyan{q}}(x_{\cyan{5}} | x_{\cyan{1}}) G_{\magenta{q}}(x_{\magenta{6}}| x_{\magenta{2}}) G_{\gray{q}}(x_{\gray{8}}|x_{\gray{4}}) \right\rangle \nonumber \\
	&+ \cyan{q}^{2} \magenta{q}^{2} \yellow{q}^{2} \gray{q} \delta_{\gray{8}\gray{4}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\cyan{5}} \periwinkle{Q}_{\magenta{6}} \periwinkle{Q}_{\yellow{7}} G_{\cyan{q}}(x_{\cyan{5}} | x_{\cyan{1}}) G_{\magenta{q}}(x_{\magenta{6}}| x_{\magenta{2}}) G_{\yellow{q}}(x_{\yellow{7}}| x_{\yellow{3}}) \right\rangle \nonumber \\
	&+ \cyan{q} \magenta{q} \yellow{q}^{2} \gray{q}^{2} \delta_{\cyan{51}} \delta_{\magenta{62}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\yellow{7}} \periwinkle{Q}_{\gray{8}} G_{\yellow{q}}(x_{\yellow{7}}| x_{\yellow{3}}) G_{\gray{q}}(x_{\gray{8}}|x_{\gray{4}}) \right\rangle \nonumber \\
	&+ \cyan{q} \magenta{q}^{2} \yellow{q} \gray{q}^{2} \delta_{\cyan{51}} \delta_{\yellow{73}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\magenta{6}} \periwinkle{Q}_{\gray{8}} G_{\magenta{q}}(x_{\magenta{6}}| x_{\magenta{2}}) G_{\gray{q}}(x_{\gray{8}}|x_{\gray{4}}) \right\rangle \nonumber \\
	&+ \cyan{q} \magenta{q}^{2} \yellow{q}^{2} \gray{q} \delta_{\cyan{51}} \delta_{\gray{8}\gray{4}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\magenta{6}} \periwinkle{Q}_{\yellow{7}} G_{\magenta{q}}(x_{\magenta{6}}| x_{\magenta{2}}) G_{\yellow{q}}(x_{\yellow{7}}| x_{\yellow{3}}) \right\rangle \nonumber \\
	&+ \cyan{q}^{2} \magenta{q} \yellow{q} \gray{q}^{2} \delta_{\magenta{62}} \delta_{\yellow{73}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\cyan{5}} \periwinkle{Q}_{\gray{8}} G_{\cyan{q}}(x_{\cyan{5}} | x_{\cyan{1}}) G_{\gray{q}}(x_{\gray{8}}|x_{\gray{4}}) \right\rangle \nonumber \\
	&+ \cyan{q}^{2} \magenta{q} \yellow{q}^{2} \gray{q} \delta_{\magenta{62}} \delta_{\gray{8}\gray{4}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\cyan{5}} \periwinkle{Q}_{\yellow{7}} G_{\cyan{q}}(x_{\cyan{5}} | x_{\cyan{1}}) G_{\yellow{q}}(x_{\yellow{7}}| x_{\yellow{3}}) \right\rangle \nonumber \\
	&+ \cyan{q}^{2} \magenta{q}^{2} \yellow{q} \gray{q} \delta_{\yellow{73}} \delta_{\gray{8}\gray{4}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\cyan{5}} \periwinkle{Q}_{\magenta{6}} G_{\cyan{q}}(x_{\cyan{5}} | x_{\cyan{1}}) G_{\magenta{q}}(x_{\magenta{6}}| x_{\magenta{2}}) \right\rangle \nonumber \\
	&+ \cyan{q}^{2} \magenta{q} \yellow{q} \gray{q} \delta_{\magenta{62}} \delta_{\yellow{73}} \delta_{\gray{8}\gray{4}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\cyan{5}} G_{\cyan{q}}(x_{\cyan{5}} | x_{\cyan{1}}) \right\rangle \nonumber \\
	&+ \cyan{q} \magenta{q}^{2} \yellow{q} \gray{q} \delta_{\cyan{51}} \delta_{\yellow{73}} \delta_{\gray{8}\gray{4}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\magenta{6}} G_{\magenta{q}}(x_{\magenta{6}}| x_{\magenta{2}}) \right\rangle \nonumber \\
	&+ \cyan{q} \magenta{q} \yellow{q}^{2} \gray{q} \delta_{\cyan{51}} \delta_{\magenta{62}} \delta_{\gray{8}\gray{4}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\yellow{7}} G_{\yellow{q}}(x_{\yellow{7}}| x_{\yellow{3}}) \right\rangle \nonumber \\
	&+ \cyan{q} \magenta{q} \yellow{q} \gray{q}^{2} \delta_{\cyan{51}} \delta_{\magenta{62}} \delta_{\yellow{73}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \periwinkle{Q}_{\gray{8}} G_{\gray{q}}(x_{\gray{8}}|x_{\gray{4}}) \right\rangle \nonumber \\
	&+ \cyan{q} \magenta{q} \yellow{q} \gray{q} \delta_{\cyan{51}} \delta_{\magenta{62}} \delta_{\yellow{73}} \delta_{\gray{8}\gray{4}} \left\langle \periwinkle{Q}_{\cyan{1}} \periwinkle{Q}_{\magenta{2}} \periwinkle{Q}_{\yellow{3}} \periwinkle{Q}_{\gray{4}} \right\rangle
\end{align}

We should substract the disconnected part,
\begin{align}
	\mathcal{D}_{\cyan{\Phi} \magenta{\Phi} \yellow{\Phi} \gray{\Phi}} = {}& \mathcal{M}_{\cyan{\Phi}} \mathcal{G}_{\magenta{\Phi} \yellow{\Phi} \gray{\Phi}} + \mathcal{M}_{\magenta{\Phi}} \mathcal{G}_{\cyan{\Phi} \yellow{\Phi} \gray{\Phi}} + \mathcal{M}_{\yellow{\Phi}} \mathcal{G}_{\cyan{\Phi} \magenta{\Phi} \gray{\Phi}} + \mathcal{M}_{\gray{\Phi}} \mathcal{G}_{\cyan{\Phi} \magenta{\Phi} \yellow{\Phi}} \nonumber\\
	&+ \mathcal{G}_{\cyan{\Phi} \magenta{\Phi}} \mathcal{G}_{\yellow{\Phi} \gray{\Phi}} + \mathcal{G}_{\cyan{\Phi} \yellow{\Phi}} \mathcal{G}_{\magenta{\Phi} \gray{\Phi}} + \mathcal{G}_{\cyan{\Phi} \gray{\Phi}} \mathcal{G}_{\magenta{\Phi} \yellow{\Phi}} \nonumber\\
	&+ \mathcal{M}_{\cyan{\Phi}} \mathcal{M}_{\magenta{\Phi}} \mathcal{G}_{\yellow{\Phi} \gray{\Phi}} + \mathcal{M}_{\yellow{\Phi}} \mathcal{M}_{\gray{\Phi}} \mathcal{G}_{\cyan{\Phi} \magenta{\Phi}} + \mathcal{M}_{\cyan{\Phi}} \mathcal{M}_{\yellow{\Phi}} \mathcal{G}_{\magenta{\Phi} \gray{\Phi}} \nonumber\\
	&+ \mathcal{M}_{\magenta{\Phi}} \mathcal{M}_{\gray{\Phi}} \mathcal{G}_{\cyan{\Phi} \yellow{\Phi}} + \mathcal{M}_{\cyan{\Phi}} \mathcal{M}_{\gray{\Phi}} \mathcal{G}_{\magenta{\Phi} \yellow{\Phi}} + \mathcal{M}_{\magenta{\Phi}} \mathcal{M}_{\yellow{\Phi}} \mathcal{G}_{\cyan{\Phi} \gray{\Phi}} \nonumber\\
	&+ \mathcal{M}_{\cyan{\Phi}} \mathcal{M}_{\magenta{\Phi}} \mathcal{M}_{\yellow{\Phi}} \mathcal{M}_{\gray{\Phi}}
\end{align}
The first and second lines involve contributions with two fragments; the third and fourth lines invovle contributions with three fragments; and the last line involves a contribution with four fragments.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Few-body Compton Correlators}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Compton correlators correspond to M{\o}ller correlators with extra external lines that involve mediating quanta. In order to restrict to elastic processes, we only consider $n$-body correlators with $2n$ external matter quanta and $2$ external mediating quanta.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{One-Body}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The one-body Compton process is described by the four-point correlator
\begin{equation}
	\mathcal{C}_{\red{\Phi} \periwinkle{A}} \equiv \lim_{\red{\varphi} \rightarrow 0} \lim_{\periwinkle{B} \rightarrow 0} \left[ \frac{\delta}{\delta \red{\varphi}^{*}(x_{\red{3}})} \frac{\delta}{\delta \periwinkle{B}(x_{\periwinkle{4}})} \frac{\delta}{\delta \red{\varphi}(x_{\red{1}})} \frac{\delta}{\delta \periwinkle{B}(x_{\periwinkle{2}})} \right] \mathcal{Z}_{\red{q}}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Two-Body}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The two-body Compton process is described by the six-point correlator
\begin{equation}
	\mathcal{C}_{\red{\Phi}\blue{\Phi} \periwinkle{A}} \equiv \lim_{\red{\varphi} \rightarrow 0} \lim_{\blue{\varphi} \rightarrow 0} \lim_{\periwinkle{B} \rightarrow 0} \left[ \frac{\delta}{\delta \red{\varphi}^{*}(x_{\red{4}})} \frac{\delta}{\delta \blue{\varphi}^{*}(x_{\blue{5}})} \frac{\delta}{\delta \periwinkle{B}(x_{\periwinkle{6}})} \frac{\delta}{\delta \red{\varphi}(x_{\red{1}})} \frac{\delta}{\delta \blue{\varphi}(x_{\blue{2}})} \frac{\delta}{\delta \periwinkle{B}(x_{\periwinkle{3}})} \right] \mathcal{Z}_{\red{q} \blue{q}}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Three-Body}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The three-body Compton process is described by the eight-point correlator
\begin{equation}
	\mathcal{C}_{\red{\Phi}\blue{\Phi}\green{\Phi} \periwinkle{A}} \equiv \left[ \frac{\delta}{\delta \red{\varphi}^{*}(x_{\red{5}})} \frac{\delta}{\delta \blue{\varphi}^{*}(x_{\blue{6}})} \frac{\delta}{\delta \green{\varphi}^{*}(x_{\green{7}})} \frac{\delta}{\delta \periwinkle{B}(x_{\periwinkle{8}})} \frac{\delta}{\delta \red{\varphi}(x_{\red{1}})} \frac{\delta}{\delta \blue{\varphi}(x_{\blue{2}})} \frac{\delta}{\delta \green{\varphi}(x_{\green{3}})} \frac{\delta}{\delta \periwinkle{B}(x_{\periwinkle{4}})} \right] \mathcal{Z}_{\red{q} \blue{q} \green{q}}
\end{equation}