% Copyright (c) 2015 Melvin Eloy Irizarry-Gelpí
\chapter{Dimensional Analysis}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
I will work with a system of units where velocity and speed are dimensionless (indeed, I will measure velocity and speed as fractions of the speed of light). This means that time and length have the same units, as well as energy, momentum, and mass:
\begin{equation}
	[\text{time}] = [\text{length}], \quad [\text{energy}] = [\text{momentum}] = [\text{mass}].
\end{equation}
Since I will be invoking the JWKB approximation, I will keep Planck's constant $2 \pi \hbar$ dimensionful. Length and mass are related via
\begin{equation}
	[\text{length}] + [\text{mass}] = [\hbar].
\end{equation}
This means that you only have two independent dimensions, which I pick to be mass and $\hbar$. Thus, an arbitrary quantity $Q$ has units
\begin{equation}
	[Q] = \Delta_{\hbar} [\hbar] + \Delta_{m} [\text{mass}],
\end{equation}
with $\Delta_{\hbar}$ the $\hbar$-dimension, and $\Delta_{m}$ the mass-dimension.

An important class of matter interactions in quantum theory is the class of local interactions. Typically, local interactions are enabled by dynamical fields that mediate between matter quanta. Because of wave-particle duality, there are valid formulations of quantum theory where the matter is either described by a mechanics action functional (leading to particle quanta), or by a field theory action functional (leading to wave quanta). As you will see below, the local interactions between particle quanta, and the local interactions between wave quanta can behave very differently in the JWKB approximation.

Classically, a wave quantum is described by a \textit{classical field theory} action functional that does not involve $\hbar$. For example, if $W$ is a scalar wave field, the contribution to the action functional from the kinetic term has the form
\begin{equation}
	\int \mathrm{d}x \left[ W(x) \left( -\frac{1}{2} \partial^{2} + \ldots \right) W(x) \right].
\end{equation}
Since any contribution to the action must have units of $\hbar$, it follows that $W$ has units
\begin{equation}
	[W] = \left( \frac{3 - D}{2} \right) [\hbar] + \left( \frac{D - 2}{2} \right) [\text{mass}],
\end{equation}
where $D$ is the number of spacetime dimensions.

On the other hand, a particle quantum is classically described by a classical mechanics action functional. This results in the corresponding field being subjected to a classical mechanic constraint. For example, if $\phi$ is a scalar particle field, the contribution to the action functional from the kinetic term has the form
\begin{equation}
	\int \mathrm{d}x \left[ \phi(x) \left( \frac{1}{2} P^{2} + \ldots \right) \phi(x) \right],
\end{equation}
where $P$ is the classical conjugate momentum operator. However, since $P \rightarrow -i \hbar \partial$, the kinetic term becomes
\begin{equation}
	\int \mathrm{d}x \left[ \phi(x) \left( -\frac{\hbar^{2}}{2} \partial^{2} + \ldots \right) \phi(x) \right].
\end{equation}
Thus, $\phi$ has units
\begin{equation}
	[\phi] = \left( \frac{1 - D}{2} \right) [\hbar] + \left( \frac{D - 2}{2} \right) [\text{mass}],
\end{equation}